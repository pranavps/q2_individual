from __future__ import division
import csv
import time
import argparse
import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from gru import CustomGRUCell
import random
import pickle

#Global variables
root_dir = '/home/cse/btech/cs1140598/'

#parsing arguments
parser = argparse.ArgumentParser(description='Test Script args')
parser.add_argument('-c', default="LSTM")
parser.add_argument('-e', type=int, default=32)
parser.add_argument('-s', type=int, default=64)
parser.add_argument('-l', default="test")
parser.add_argument('-m', default="model.pth")
parser.add_argument('-p',default="test")
args = parser.parse_args()

#model's experimental parameters
cell_type = args.c
EMBEDDING_DIM = args.e
HIDDEN_DIM = args.s
folder_name = args.l
path = args.p
model_path = args.m

#helper functions

def getData(folder,name):
	data = []
	label = []

	file = open(root_dir + folder + "/" + path + "_sentences.txt", "r")
	for line in file:
		data.append(line.split())

	file = open(root_dir + folder + "/" + path + "_tags.txt", "r")
	for line in file:
		label.append(line.split())

	return (data,label)


def prepare_sequence(seq, to_ix):
    idxs = []
    for w in seq:
    	if to_ix.get(w)!=None:
    		idxs.append(to_ix[w])
    	else:
    		idxs.append(random.randint(0,len(to_ix)))
    tensor = torch.LongTensor(idxs)
    if torch.cuda.is_available():
    	tensor = tensor.cuda()
    return tensor

def getSequence(data,word_to_ix,label,tag_to_ix):
	data_new = []
	label_new = []
	for i in range(len(data)):
		data_new.append(prepare_sequence(data[i], word_to_ix))
		label_new.append(prepare_sequence(label[i], tag_to_ix))	
	return (data_new, label_new)

class tagger(nn.Module):
	def __init__(self, embedding_dim, hidden_dim, vocab_size, tagset_size, cell_type = "LSTM"):
		super(tagger, self).__init__()
		self.hidden_dim = hidden_dim

		self.word_embeddings = nn.Embedding(vocab_size, embedding_dim)
		
		if cell_type == "LSTM":
			self.lstm = nn.LSTMCell(embedding_dim, hidden_dim)
		elif cell_type == "GRU":
			self.lstm = nn.GRUCell(embedding_dim, hidden_dim)
		elif cell_type == "RNN":
			self.lstm = nn.RNNCell(embedding_dim, hidden_dim)
		else:
			self.lstm = CustomGRUCell(embedding_dim, hidden_dim)

		self.hidden2tag = nn.Linear(hidden_dim, tagset_size)
		self.hidden = self.init_hidden(cell_type)

	def init_hidden(self,cell_type):
		tensor = torch.zeros( 1, self.hidden_dim)
		if torch.cuda.is_available():
			tensor = tensor.cuda()
		variable = autograd.Variable(tensor)
		if cell_type == "LSTM":
			return (variable,variable)
		else:
			return (variable)

	def forward(self, sentence):
		embeds = self.word_embeddings(sentence)
		output = []
		for word in embeds:
			self.hidden = self.lstm(word.view(1, -1), self.hidden)
			output.append(self.hidden[0])
		output = torch.stack(output)
		tag_space = self.hidden2tag(output.view(len(sentence), -1))
		tag_scores = F.log_softmax(tag_space)
		return tag_scores

def validation(model,val_data,val_label):
	total = 0
	correct = 0
	
	for i in range(len(val_data)):
		input_data = Variable(val_data[i])
		input_tag = val_label[i]
		tag_scores = model(input_data)
		_, predicted = torch.max(tag_scores.data, 1)
		total += len(input_tag)
		correct += (predicted == input_tag).sum()

	return (100*(float(correct)/float(total)))


if __name__ == '__main__':
	test_data = getData(folder_name,path)
	
	pickle_in = open(root_dir + "data_dictionary.pickle","rb")
	word_to_ix = pickle.load(pickle_in)
	pickle_in = open(root_dir + "label_dictionary.pickle","rb")
	tag_to_ix = pickle.load(pickle_in)

	(test_data, test_label) = getSequence(test_data[0], word_to_ix, test_data[1], tag_to_ix)

	model = tagger(EMBEDDING_DIM, HIDDEN_DIM, len(word_to_ix), len(tag_to_ix), cell_type)
	
	if torch.cuda.is_available():
		model = model.cuda()

	if torch.cuda.is_available():
		model = model.cuda()
	
	model.load_state_dict(torch.load(model_path))

	acc = validation(model,test_data,test_label)
	print('Accuracy of the network on the test data set: %f %%' % (acc))