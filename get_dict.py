import pickle

#Global variables
root_dir = '/home/cse/btech/cs1140598/'

def getData(path):
	data = []
	label = []

	file = open(root_dir + "RNN_Data_files/" + path + "_sentences.txt", "r")
	for line in file:
		data.append(line.split())

	file = open(root_dir + "RNN_Data_files/" + path + "_tags.txt", "r")
	for line in file:
		label.append(line.split())

	return (data,label)

def fillDict(data,dict):
	for sent in data:
	    for word in sent:
	        if word not in dict:
	            dict[word] = len(dict)

def getDict(train,val):
	word_to_ix = {}
	tag_to_ix = {}
	train_data, train_label = train
	val_data, val_label = val
	fillDict(train_data,word_to_ix)
	fillDict(val_data,word_to_ix)
	fillDict(train_label,tag_to_ix)
	fillDict(val_label,tag_to_ix)
	return (word_to_ix,tag_to_ix)


if __name__ == '__main__':
	train_data = getData('train')
	val_data = getData('val')
	
	(word_to_ix,tag_to_ix) = getDict(train_data,val_data)
	
	pickle_out = open(root_dir + "data_dictionary.pickle","wb")
	pickle.dump(word_to_ix, pickle_out)
	pickle_out.close()
	pickle_out = open(root_dir + "label_dictionary.pickle","wb")
	pickle.dump(tag_to_ix, pickle_out)
	pickle_out.close()
